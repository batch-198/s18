// function able to receive data without the use of global variables without the use of global variable or prompt();

// "name" - is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked
function printName(name){
	console.log("My name is " + name);
}

// Data passed into a function invocation can be received by the function
// This is what we call an argument
printName("Juana");

// Data passed into the function through invocation is called argument
// The argument is then stored as parameter

function printMyAge(age){
	console.log("I am " + age);
}

printMyAge(25);
printMyAge();

// console.log(age);
// ^ Result: Error, age parameter is local scope


// check divisibility reusably using a function with arguments and parameters
function checkDivisibilityby8(num){

	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleby8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleby8);

}

checkDivisibilityby8(64);
checkDivisibilityby8(27);

// Mini Activity
function mySuperhero(superhero){
	console.log("my favorite superhero is " + superhero);
}

mySuperhero("wonderwoman");

function printMyFavoriteLanguage(language){
	console.log("My favorite language is: " + language);
}

printMyFavoriteLanguage("Javacript");
printMyFavoriteLanguage("Java");

// Multiple Arguments can also be passed into a function; Multiple paraments can contain our arguments.

function printFullName(firstName,middleName,lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("Juan","Crisostomo","Ibarra");
/*
	Parameters will contain the arguments according to the order it was passed.

	In other languages, providing more/less arguments than expected parameters sometimes causes an error or changes the behavior of the function

	in Javascript, we don't have to worry about that.

	In Javascript, providing less arguments than the expected parameters will automatically assign an undefined value to the parameter
*/

printFullName("Stephen","Wardell");

// Use Variables as Arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName,mName,lName);

function favoriteSongs(song1,song2,song3,song4,song5){
	console.log("my favorite songs are:");
	console.log(song1);
	console.log(song2);
	console.log(song3);
	console.log(song4);
	console.log(song5);

	// return song1 + song2 + song3 + song4 + song5;
}
	
	favoriteSongs("You Outside My Window","Tokyo","Graduation","INVU","Heart");

// Return Statement
// Currently or so far, our functions are able to display data in our console
// However, our function cannot yet return values. Functions are able to return values which can be saved into a variable using the return statement/keyword

let fullName = printFullName("Mark","Joseph","Lacdao");
console.log(fullName);

function returnFullName(firstName,middleName,lastName){
	return firstName + " " + middleName + " " + lastName;
}

fullName = returnFullName("Ernesto","Antonio","Maceda");
console.log(fullName);

// Functions which have a return statement are able to return value and it can be saved in a variable.

console.log(fullName + "is my grandpa.");

function returnPhilippineAddress(city){

	return city + ",Philippines" 
}

// return - return a value from a function which we can save in a variable
let myFullAddress = returnPhilippineAddress("Cainta");
console.log(myFullAddress);

/* Syntax:

function functionName(parameter){
return parameter
}

let variableName = functionName("value");

console.log(variableName);*/


// returns true if number is divisible by 4, else, returns false
function checkDivisibilityBy4(num){

	let remainder = num % 4;

	let isDivisibleBy4 = remainder === 0;

	// returns either true or false
	// not only can you return raw values/data, you can also directly return a variable
	return isDivisibleBy4;
	// Return keyword not only allows us to retun value but also ends the process of our function.
	console.log("I am run after the return.")
}

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);

console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);

// Mini Activity

function createPlayerInfo(username,level,job){

	return "username: "+ username + ", level: " + level + ", job: " + job
}

let user1 = createPlayerInfo("white_night",95,"Paladin");
console.log(user1);