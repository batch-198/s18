function additionFunction(num1,num2){
	console.log("Displayed the sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
}

additionFunction(5,15);

function subtractionFunction(num1,num2){
	console.log("Displayed the difference of " + num1 + " and " + num2);
	console.log(num1 - num2);
}

subtractionFunction(20,15);

function multiplyFunction(num1,num2){
	console.log("The product of " + num1 + " and " + num2);

	let result = num1 * num2;
	return result
}

let product = multiplyFunction(50,10);
console.log(product);

function divisionFunction(num1,num2){
	console.log("The quotient of " + num1 + " and " + num2);

	let result = num1 / num2
	return result
}

let quotient = divisionFunction(50,10);
console.log(quotient);

function circle(radius){
	let result = 3.1416 * (radius ** 2);
	return result
}

let circleArea = circle(15);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

function average(num1,num2,num3,num4){
	let result = (num1 + num2 + num3 + num4) / 4;
	return result
}

let averageVar = average(20,40,60,80);
console.log("The average of 20, 40, 60, and 80:");
console.log(averageVar);


function passingGrade(score,total){
	let percentage = score / total * 100;
	let isPassed = percentage >= 75;

	return isPassed;
}

let grade = passingGrade(38,50);
console.log("is 38/50 a passing score?");
console.log(grade);